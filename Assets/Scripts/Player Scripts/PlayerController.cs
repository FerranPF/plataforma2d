﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController: MonoBehaviour {

	public float maxSpeed = 10.0f;
	public float speed = 2.0f;
	public bool isGrounded;
	public float jumpPower = 15f;
	public Transform resPos;
	public bool gameOver = false;
	public bool win = false;
	public AudioSource deadSound;
	public bool key = false;
	public GameObject keyObj;

	private BoxCollider2D col;
	private Rigidbody2D rb2d;
	private Animator anim;
	private bool jump;
	private bool doubleJump;
	private bool dead = false;
	public int lives = 3;


	void Awake () {
		col = GetComponent<BoxCollider2D>();
		rb2d = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		anim.SetBool ("isDead", dead);
	}


	void Update(){
		anim.SetFloat ("Speed", Mathf.Abs(rb2d.velocity.x));
		anim.SetBool ("isGrounded", isGrounded);

		if (isGrounded) doubleJump = true;

		if (!dead) {
			
			if (Input.GetKeyDown (KeyCode.UpArrow)) {

				if (isGrounded) {
					jump = true;
					doubleJump = true;
				} else if (doubleJump) {
					jump = true;
					doubleJump = false;
				}
			}
		}

	}


	void FixedUpdate () {

		Vector3 fixedVelocity = rb2d.velocity;
		fixedVelocity.x *= 0.98f;

		if (isGrounded) {
			rb2d.velocity = fixedVelocity;
		}

		float h = Input.GetAxis ("Horizontal");

		rb2d.AddForce (Vector2.right * speed * h);

		float limitedSpeed = Mathf.Clamp (rb2d.velocity.x, -maxSpeed, maxSpeed);

		rb2d.velocity = new Vector2 (limitedSpeed, rb2d.velocity.y);

		if(h > 0.1f){
			transform.localScale = new Vector3 (1f, 1f, 1f);
		}

		if(h < -0.1f){
			transform.localScale = new Vector3 (-1f, 1f, 1f);
		}

		if (jump) {
			rb2d.AddForce (Vector2.up * jumpPower, ForceMode2D.Impulse);
			jump = false;
		}
	}

	void OnCollisionEnter2D(Collision2D col){
		if(col.gameObject.CompareTag("Enemy")){
			Dead ();
		}

		if (col.gameObject.CompareTag ("Lock")) {
			if(key == true){
				win = true;
				gameOver = true;
			}
		}

		if(col.gameObject.CompareTag("Key")){
			keyObj.SetActive(false);
			key = true;
		}

	}

	void Dead(){
		dead = true;
		anim.SetBool ("isDead", dead);
		col.enabled = false;
		deadSound.Play ();
		if (lives == 0) {
			gameOver = true;
		} else {
			lives--;
		}
		Invoke ("Respawn", 0.2f);
	}

	public void Respawn(){
		dead = false;
		anim.SetBool ("isDead", dead);
		col.enabled = true;
		transform.position = resPos.position;
	}


}