﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	public GameObject target;
	public float smoothTime;

	private Vector2 velocity;
	void Start () {
		
	}
	
	void FixedUpdate () {
		float posX = Mathf.SmoothDamp(transform.position.x,target.transform.position.x, ref velocity.x, smoothTime);
		float posY = Mathf.SmoothDamp(transform.position.y,target.transform.position.y, ref velocity.y, smoothTime);

		transform.position = new Vector3 (posX, posY, transform.position.z);
	}
}
