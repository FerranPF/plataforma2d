﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	private GameObject player;
	private PlayerController playerCont;
	public GameObject canvas;
	public GameObject win;
	public GameObject loose;
	private bool pause = false;

	void Start(){
		Time.timeScale = 1;
		FindPlayer ();
		canvas.SetActive (false);
		win.SetActive (false);
		loose.SetActive (false);
	}

	void FindPlayer(){
		player = GameObject.Find ("Player");
		playerCont = player.GetComponent<PlayerController> ();
	}

	void Update(){

		if (Input.GetKeyDown (KeyCode.Escape) && pause == false) {
			canvas.SetActive (true);
			pause = true;
			Time.timeScale = 0;
		} else if (Input.GetKeyDown (KeyCode.Escape) && pause == true) {
			canvas.SetActive (false);
			pause = false;
			Time.timeScale = 1;
		}
		
		if (playerCont.gameOver == true) {
			canvas.SetActive (true);
			Time.timeScale = 0;
			if (playerCont.win == true) {
				win.SetActive (true);
			}else if(playerCont.win == false){
				loose.SetActive (true);
			}
		}
	
	}

	public void MainMenu(){
		Time.timeScale = 1;
		SceneManager.LoadScene ("Menu");
	}
}
