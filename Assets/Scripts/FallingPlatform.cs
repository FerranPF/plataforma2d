﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatform : MonoBehaviour {

	public float fallDelay = 1.0f;
	public float respawnDelay = 5.0f;

	private Rigidbody2D rb2d;
	private BoxCollider2D bc2d;
	private Vector3 startPos;

	void Start () {
		rb2d = GetComponent<Rigidbody2D> ();	
		bc2d = GetComponent<BoxCollider2D> ();	
		startPos = transform.position;
	}
	
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D col){

		if(col.gameObject.CompareTag("Player")){
			Invoke ("Fall", fallDelay);
			Invoke ("Respawn", fallDelay + respawnDelay);
		}

	}

	void Fall(){
		rb2d.isKinematic = false;
		bc2d.isTrigger = true;
	}

	void Respawn(){
		transform.position = startPos;
		rb2d.isKinematic = true;
		rb2d.velocity = Vector3.zero;
		bc2d.isTrigger = false;
	}
}
