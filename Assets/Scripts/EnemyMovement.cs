﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

	private EnemyController enemy;

	void Start(){
		enemy = GetComponentInParent<EnemyController> ();
	}

	void Update(){
		
	}

	void OnCollisionEnter2D(Collision2D col){
		if(col.gameObject.CompareTag("Ground")){
			enemy.speed = -enemy.speed;
		}

	}
}
